from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin, LoginManager
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
login = LoginManager()


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(60), unique=True)
    password_hash = db.Column(db.String(128))
    email = db.Column(db.String(120), unique=True)
    films = db.relationship('Film', backref='user', lazy=True)
    ratings = db.relationship('Rating', backref='user', lazy=True)

    def __init__(self, username, email):
        self.username = username
        self.email = email
        self.set_password("1")

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    films = db.relationship('Film', backref='category', lazy=True)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Category {}>'.format(self.name)


class Film(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    author = db.Column(db.String(128), nullable=False)
    year = db.Column(db.String(4), nullable=False)
    description = db.Column(db.String(500), nullable=True)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'), nullable=False)
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False)
    poster_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __init__(self, name, category_id, country_id, poster_id):
        self.name = name
        self.category_id = category_id
        self.country_id = country_id
        self.poster_id = poster_id

    def __repr__(self):
        return '<Film {}>'.format(self.name)


class Country(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    films = db.relationship('Film', backref='country', lazy=True)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Country {}>'.format(self.name)


class Rating(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    rev_stars = db.Column(db.Integer, nullable=False)
    rev_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    rev_comment = db.Column(db.String(500), nullable=True)

    def __init__(self, stars, comment, rev_id):
        self.rev_stars = stars
        self.rev_comment = comment
        self.rev_id = rev_id

    def __repr__(self):
        return '<Rating {}, {}>'.format(self.rev_id, self.rev_stars)


# load user to memory
# reload user object from the user ID stored in the session
@login.user_loader
def load_user(_id):
    return User.query.get(int(_id))
